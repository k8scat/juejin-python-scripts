import logging
import random
import requests
from config import config


def get_proxy():
    if random.random() > 0.5:
        logging.info('Get proxy from proxypool')
        with requests.get(config.PROXY_POOL_API) as r:
            if r.status_code != requests.codes.OK or r.text == '':
                logging.error(f'Failed to get proxy: {r.text}')
                return None
            return r.text
    else:
        logging.info('Get proxy from proxy_pool')
        with requests.get(config.PROXY_POOL_API2) as r:
            if r.status_code != requests.codes.OK:
                return None
            res = r.json()
            if not res['last_status']:
                return None
            return res['proxy'] if res['proxy'] else None

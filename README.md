# 掘金 juejin.com

## Auth Cookie

掘金网站的 cookies 在退出登录后就会失效，有两种方案可以 cookies 有效：

- 清空浏览器 cookies 进行退出登录
- 使用无痕模式进行登录

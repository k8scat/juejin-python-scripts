CREATE TABLE `article_view` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `url` TEXT NOT NULL,
    `count` INTEGER NOT NULL,
    `create_time` INTEGER NOT NULL
);
import logging
from api import client


def get_user():
    endpoint = '/user_api/v1/user/get'
    res = client.get(endpoint)
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to get user: {res["err_msg"] if res is not None else ""}')
        return None
    return res['data']

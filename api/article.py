from config import config
import logging

import requests
from api import client
import json


def list_recommend_articles(cursor='0'):
    endpoint = '/recommend_api/v1/article/recommend_all_feed'
    data = {
        "id_type": 2,
        "client_type": 2608,
        "sort_type": 200,
        "cursor": cursor,
        "limit": 20
    }
    res = client.post(endpoint, data)
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to list recommend articles: {res["err_msg"] if res is not None else ""}')
        return None
    return res


def list_user_articles_with_cursor(user_id, cursor, sort_type=2):
    endpoint = '/content_api/v1/article/query_list'
    data = {
        'user_id': user_id,
        'sort_type': sort_type,
        'cursor': cursor
    }
    res = client.post(endpoint, data)
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to list user articles with cursor: {res["err_msg"] if res is not None else ""}')
        return None
    return res


def list_user_all_articles(user_id):
    count = 1
    cursor = '0'
    all_articles = []
    while int(cursor) < count:
        res = list_user_articles_with_cursor(user_id, cursor)
        if res is None:
            return None
        count = res['count']
        cursor = res['cursor']
        all_articles.extend(res['data'])
    return all_articles


def get_article_detail(article_id, proxies=None, cookie=''):
    endpoint = '/content_api/v1/article/detail'
    data = {
        'article_id': article_id
    }
    res = client.post(endpoint, data, proxies=proxies, cookie=cookie)
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to get article detail: {res["err_msg"] if res is not None else ""}')
        return None
    return res['data']


# 点赞文章
def digg_article(account_cookie, article_id, proxies=None):
    endpoint = '/interact_api/v1/digg/save'
    data = {
        "client_type": 2608,
        "item_id": article_id,
        "item_type": 2
    }
    res = client.post(endpoint, data, proxies=proxies, cookie=account_cookie)
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to digg article ( {config.JUEJIN_ARTICLE_BASE_URL}{article_id} ) : {res["err_msg"] if res is not None else ""}')

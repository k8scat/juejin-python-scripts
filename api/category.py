from api import client
import logging


def list_all_categories():
    endpoint = '/tag_api/v1/query_category_list'
    res = client.post(endpoint, cookie='')
    if res is None or res['err_no'] != 0:
        logging.error(
            f'Failed to list all categories: {res["err_msg"] if res is not None else ""}')
        return None
    return res['data']

import logging
import random
from config import config
import requests


def get(endpoint, proxies=None, cookie=config.JUEJIN_COOKIE):
    headers = config.JUEJIN_COMMON_HEADERS.copy()  # shallow copy
    headers['User-Agent'] = random.choice(config.UA_LIST)
    if cookie != '':
        headers['Cookie'] = cookie
    api = f'{config.JUEJIN_BASE_API}{endpoint}'
    with requests.get(api, headers=config.JUEJIN_COMMON_HEADERS, proxies=proxies) as r:
        if r.status_code != requests.codes.ok:
            logging.error(f'GET {api} {r.status_code}: {r.text}')
            return None
        return r.json()


def post(endpoint, data, proxies=None, cookie=config.JUEJIN_COOKIE):
    headers = config.JUEJIN_COMMON_HEADERS.copy()  # shallow copy
    headers['User-Agent'] = random.choice(config.UA_LIST)
    if cookie != '':
        headers['Cookie'] = cookie
    api = f'{config.JUEJIN_BASE_API}{endpoint}'
    with requests.post(api, headers=config.JUEJIN_COMMON_HEADERS, json=data, proxies=proxies) as r:
        if r.status_code != requests.codes.ok:
            logging.error(f'POST {api} {r.status_code}: {r.text}')
            return None
        return r.json()

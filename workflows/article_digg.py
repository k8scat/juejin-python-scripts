import logging
import sys
from utils import proxy
from config import config
from api import article


def main():
    to_digg_articles = article.list_user_all_articles(config.JUEJIN_USER_ID)
    if to_digg_articles is None:
        sys.exit(1)

    for account in config.JUEJIN_ACCOUNT_COOKIE_LIST:
        for to_digg_article in to_digg_articles:
            # 使用代理，防止 IP 被封
            proxy_addr = proxy.get_proxy()
            if proxy_addr is None:
                continue
            proxies = {
                'http': proxy_addr,
                'https': proxy_addr
            }
            logging.info(f'Using proxies: {proxies}')

            # 判断是否已经点赞，如果已经点赞，则跳过
            account_cookie = account['cookie']
            article_id = to_digg_article['article_id']
            if has_digg(account_cookie, proxies=proxies, cookie=article_id):
                continue

            # 从首页推荐文章中获取没有点赞的文章列表
            articles_without_digg = list_articles_without_digg()
            for a in articles_without_digg:
                aid = a['article_id']
                logging.info(
                    f'Digg article: {config.JUEJIN_ARTICLE_BASE_URL}{aid}')
                article.digg_article(account_cookie, aid, proxies=proxies)

            logging.info(
                f'Digg article: {config.JUEJIN_ARTICLE_BASE_URL}{article_id}')
            article.digg_article(account_cookie, article_id, proxies=proxies)


def has_digg(account_cookie, article_id, proxies=None):
    article_detail = article.get_article_detail(
        article_id, proxies=proxies, cookie=account_cookie)
    return article_detail['user_interact']['is_digg']


def list_articles_without_digg(account_cookie, count=20, proxies=None):
    articles = []
    recommend_articles = article.list_recommend_articles()
    for a in recommend_articles:
        a = a['item_info']
        if has_digg(account_cookie, a['article_id'], proxies=proxies):
            continue
        articles.append(a)
        if len(articles) >= count:
            return articles

from config import config
from utils import proxy
import logging
import sys
import random
from db import db, article_view
from api import user, article


def main():
    # 使用代理，防止 IP 被封
    proxy_addr = proxy.get_proxy()
    if proxy_addr is None:
        sys.exit(1)
    proxies = {
        'http': proxy_addr,
        'https': proxy_addr
    }
    logging.info(f'Using proxies: {proxies}')

    # 获取 cookies 对应的用户信息
    user_info = user.get_user()
    if user_info is None:
        sys.exit(1)

    # 获取用户的所有文章
    user_id = user_info['user_id']
    articles = article.list_user_all_articles(user_id)
    if articles is None:
        sys.exit(1)
    logging.info(f'Found articles count: {len(articles)}')

    db_conn = db.get_db(config.SQLITE3_DB_FILE)
    try:
        for article_info in articles:
            article_id = article_info['article_id']
            view_count = article_info['article_info']['view_count']
            if ignore_view(article_id, view_count, article_info):
                continue

            article_url = f'{config.JUEJIN_ARTICLE_BASE_URL}{article_id}'
            last_view_count = \
                article_view.get_last_view_count(db_conn, article_url)
            article_detail = article.get_article_detail(article_id)
            if article_detail is None:
                continue
            article_view.insert(db_conn, article_url, view_count)
            msg = f'View article: {article_url}, current view count is {view_count}'

            if last_view_count is not None:
                msg += f' and last view count: {last_view_count}'
            logging.info(msg)
    finally:
        if db_conn is not None:
            db_conn.close()


def ignore_view(article_id: str, view_count: int, article_info: dict) -> bool:
    # 白名单中的文章优先级最高，不受其他约束
    if article_id in config.JUEJIN_ARTICLE_VIEW_WHITELIST:
        logging.info(
            f'Article in whitelist: {config.JUEJIN_ARTICLE_BASE_URL}{article_id}')
        return False

    # 访问量超过设定的最大访问量时，不再访问该文章
    if view_count > config.JUEJIN_ARTICLE_VIEW_MAX:
        logging.info(
            f'Article view limited({config.JUEJIN_ARTICLE_VIEW_MAX}): {config.JUEJIN_ARTICLE_BASE_URL}{article_id}')
        return True

    # 黑名单中的文章不再访问
    if article_id in config.JUEJIN_ARTICLE_VIEW_BLACKLIST:
        logging.info(
            f'Article in blacklist: {config.JUEJIN_ARTICLE_BASE_URL}{article_id}')
        return True

    # 审核未通过
    if article_info['article_info']['audit_status'] != 2:
        logging.info(
            f'Article under audit: {config.JUEJIN_ARTICLE_BASE_URL}{article_id}')
        return True

    # 随机访问
    return random.random() > 0.9

import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

UA_LIST = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/4E423F',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
    'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
    'Opera/9.80 (Windows NT 5.1; U; zh-sg) Presto/2.9.181 Version/12.00',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it-IT) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4'
]


PROXY_POOL_API = os.environ.get(
    'PROXY_POOL_API', 'http://127.0.0.1:5555/random')
PROXY_POOL_API2 = os.environ.get(
    'PROXY_POOL_API2', 'http://127.0.0.1:5010/get/')

JUEJIN_COOKIE = 'n_mh=dGQkf9HU3ND5UfHfwBJbRzdzIM0m4dH1T6DrYezJl20; _ga=GA1.2.594049184.1612534030; passport_csrf_token_default=12289b132a2154807b7112a0d00a1562; passport_csrf_token=12289b132a2154807b7112a0d00a1562; sid_guard=9f4686a129fbeb2b8c42bc0f6f8f90f9%7C1622733651%7C5184000%7CMon%2C+02-Aug-2021+15%3A20%3A51+GMT; uid_tt=657a042df22a59da293f2003d8c31eb1; uid_tt_ss=657a042df22a59da293f2003d8c31eb1; sid_tt=9f4686a129fbeb2b8c42bc0f6f8f90f9; sessionid=9f4686a129fbeb2b8c42bc0f6f8f90f9; sessionid_ss=9f4686a129fbeb2b8c42bc0f6f8f90f9; MONITOR_WEB_ID=076ac22b-79ad-4ba2-91b5-8ebf6e7360f0; _tea_utm_cache_2608={%22utm_source%22:%2220210528%22%2C%22utm_medium%22:%22Ccenter%22%2C%22utm_campaign%22:%2230day%22}'
JUEJIN_BASE_API = 'https://api.juejin.cn'
JUEJIN_COMMON_HEADERS = {
    'Cookie': JUEJIN_COOKIE,
    'Origin': 'https://juejin.cn',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
    'Referer': 'https://juejin.cn/'
}
# https://juejin.cn/post/6977534460027895815
JUEJIN_ARTICLE_BASE_URL = 'https://juejin.cn/post/'
JUEJIN_ARTICLE_VIEW_WHITELIST = []
JUEJIN_ARTICLE_VIEW_BLACKLIST = []
JUEJIN_ARTICLE_VIEW_MAX = 2000

SQLITE3_SCHEMA_FILE = os.path.join(ROOT_DIR, 'schema-sqlite3.sql')
SQLITE3_DB_FILE = os.path.join(ROOT_DIR, 'juejin-data.sqlite3')

JUEJIN_USER_ID = '588993963244430'

JUEJIN_ACCOUNT_COOKIE_LIST = [
    {
        'phone': '17770040362',
        'cookie': 'passport_csrf_token_default=1ffd38b6bfe6c57449488bbb6711c22a; passport_csrf_token=1ffd38b6bfe6c57449488bbb6711c22a; sid_guard=b8c9415b52d2db959f36a71032457a89%7C1624632010%7C5184000%7CTue%2C+24-Aug-2021+14%3A40%3A10+GMT; uid_tt=f56cae1ae79f1aaf3ae7da1130372cb5; uid_tt_ss=f56cae1ae79f1aaf3ae7da1130372cb5; sid_tt=b8c9415b52d2db959f36a71032457a89; sessionid=b8c9415b52d2db959f36a71032457a89; sessionid_ss=b8c9415b52d2db959f36a71032457a89; n_mh=0QZ6phTqcEmPnqWxo6aojGvuUMgMoAtf09jXaM2nV9M; MONITOR_WEB_ID=6f8203c0-cd60-4fac-95b8-4b8f54a08f58'
    },
    {
        'phone': '13970069241',
        'cookie': 'passport_csrf_token_default=ffec642dfab34374d1aee2263a3b2ccf; passport_csrf_token=ffec642dfab34374d1aee2263a3b2ccf; sid_guard=5f94ded9b74e840c32d38a66bf43fe45%7C1624656388%7C5184000%7CTue%2C+24-Aug-2021+21%3A26%3A28+GMT; uid_tt=d489d53bf17a1015374bdf231e2b587d; uid_tt_ss=d489d53bf17a1015374bdf231e2b587d; sid_tt=5f94ded9b74e840c32d38a66bf43fe45; sessionid=5f94ded9b74e840c32d38a66bf43fe45; sessionid_ss=5f94ded9b74e840c32d38a66bf43fe45; n_mh=XrKvXbqRL_4b3lqm6lgOIkEMa4c4UPgDipA_jm5Ckn0; MONITOR_WEB_ID=f1b464af-a76f-4913-8025-d80a800ab7e6'
    },
    {
        'phone': '15216267867',
        'cookie': 'passport_csrf_token_default=9907aee14c68f3f3319a6429bb30014c; passport_csrf_token=9907aee14c68f3f3319a6429bb30014c; sid_guard=e03cb21c48ee8f3b5b7487ea3b208027%7C1624656509%7C5184000%7CTue%2C+24-Aug-2021+21%3A28%3A29+GMT; uid_tt=66a95c5b9c6e37cbad1061fd5272db02; uid_tt_ss=66a95c5b9c6e37cbad1061fd5272db02; sid_tt=e03cb21c48ee8f3b5b7487ea3b208027; sessionid=e03cb21c48ee8f3b5b7487ea3b208027; sessionid_ss=e03cb21c48ee8f3b5b7487ea3b208027; n_mh=pAZjOlTvsQJ7h5xNzMT5Xonx3hSYN7huFE6IkHsOxow; MONITOR_WEB_ID=361985a4-6c34-4dae-acf6-4d6a35b72661'
    },
    {
        'phone': '15297949907',
        'cookie': 'passport_csrf_token_default=27020ab95971c68b6584e8b8b6d90251; passport_csrf_token=27020ab95971c68b6584e8b8b6d90251; sid_guard=870dd3ac13a0d06545722f6e545faca9%7C1624656597%7C5184000%7CTue%2C+24-Aug-2021+21%3A29%3A57+GMT; uid_tt=5a2188ebac0f4a50052f0e6f3a41b1ea; uid_tt_ss=5a2188ebac0f4a50052f0e6f3a41b1ea; sid_tt=870dd3ac13a0d06545722f6e545faca9; sessionid=870dd3ac13a0d06545722f6e545faca9; sessionid_ss=870dd3ac13a0d06545722f6e545faca9; n_mh=Hp_-9hGB65z5KJpdKfdGhbhF2krRJuiEUGBvudpha-E; MONITOR_WEB_ID=b1f0536a-8b56-4ab3-9218-cc9328133e81'
    },
    {
        'phone': '15679195693',
        'cookie': 'passport_csrf_token_default=e226686239a899f72c89eab6882236d0; passport_csrf_token=e226686239a899f72c89eab6882236d0; sid_guard=b8d7ee8d429e21ae0ba4ba677eb44174%7C1624656676%7C5184000%7CTue%2C+24-Aug-2021+21%3A31%3A16+GMT; uid_tt=5fe5d23830b6a071a9429b717533a3fd; uid_tt_ss=5fe5d23830b6a071a9429b717533a3fd; sid_tt=b8d7ee8d429e21ae0ba4ba677eb44174; sessionid=b8d7ee8d429e21ae0ba4ba677eb44174; sessionid_ss=b8d7ee8d429e21ae0ba4ba677eb44174; n_mh=8YhQenwTuUYkUN7VPq5o1ZxzAOsx3pivToBJU6A1QiY; MONITOR_WEB_ID=9982e8d7-e9ab-4fc9-9868-23de0ab97ef1'
    },
    {
        'phone': '18091354428',
        'cookie': 'passport_csrf_token_default=734414e61787c2cc05f20614924cadaa; passport_csrf_token=734414e61787c2cc05f20614924cadaa; sid_guard=3f410dc20e9022a0125cc44cdb234b2f%7C1624656742%7C5184000%7CTue%2C+24-Aug-2021+21%3A32%3A22+GMT; uid_tt=73bbfb74cb5d2e9c8f8d70218c22520e; uid_tt_ss=73bbfb74cb5d2e9c8f8d70218c22520e; sid_tt=3f410dc20e9022a0125cc44cdb234b2f; sessionid=3f410dc20e9022a0125cc44cdb234b2f; sessionid_ss=3f410dc20e9022a0125cc44cdb234b2f; n_mh=yJcmEMYKQ6lfJ4qsPwKkn_booiGlPJHETELoJ3APy34; MONITOR_WEB_ID=bc89194c-3423-40d0-a7ff-32d5744c2e95'
    },
    {
        'phone': '18391193629',
        'cookie': 'passport_csrf_token_default=3ec15682a018b269708ec09558a75e44; passport_csrf_token=3ec15682a018b269708ec09558a75e44; sid_guard=9280215ba9daa7253101ca78fd7eb099%7C1624656815%7C5184000%7CTue%2C+24-Aug-2021+21%3A33%3A35+GMT; uid_tt=bff3d2165f4a788c594dc2fb323d05bb; uid_tt_ss=bff3d2165f4a788c594dc2fb323d05bb; sid_tt=9280215ba9daa7253101ca78fd7eb099; sessionid=9280215ba9daa7253101ca78fd7eb099; sessionid_ss=9280215ba9daa7253101ca78fd7eb099; n_mh=14Bi6rW7t2NWWmuTY4OIT_rtLAI0-acW732zNB36CAw; MONITOR_WEB_ID=91a3d2ea-63b4-471e-91cf-a0fca3e02cdc'
    },
    {
        'phone': '18897919281',
        'cookie': 'passport_csrf_token_default=68ba502094e65b805d80ca945a35fd6f; passport_csrf_token=68ba502094e65b805d80ca945a35fd6f; sid_guard=91e381ec8cd9c24016a467faeaaca5d6%7C1624656888%7C5184000%7CTue%2C+24-Aug-2021+21%3A34%3A48+GMT; uid_tt=062b1824603b9edff0abf8e0bb004df2; uid_tt_ss=062b1824603b9edff0abf8e0bb004df2; sid_tt=91e381ec8cd9c24016a467faeaaca5d6; sessionid=91e381ec8cd9c24016a467faeaaca5d6; sessionid_ss=91e381ec8cd9c24016a467faeaaca5d6; n_mh=KYgqJFrmghK3JSxGMllFCo0XjsKp-7DHLwz70cbuD38; MONITOR_WEB_ID=46d248e0-e642-4ab5-9bb8-68df1db79617'
    }
]

import logging
import sys
from workflows import article_view, article_digg

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(filename)s: %(levelname)s %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")


def main(action):
    if action == 'article_view':
        article_view.main()
    elif action == 'article_view':
        article_digg.main()


if __name__ == '__main__':
    if len(sys.argv) != 2:
        logging.error('Invalid action!')
        sys.exit(1)
    main(sys.argv[1])

FROM python:3.6-alpine
LABEL maintainer="K8sCat <k8scat@gmail.com>"
WORKDIR /juejin-python-scripts
COPY . .
RUN pip install -r requirements.txt
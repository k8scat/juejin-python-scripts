import sqlite3
import logging
import os
from config import config


def get_db(db_file: str) -> sqlite3.Connection:
    need_init = False
    if not os.path.exists(db_file):
        need_init = True

    conn = sqlite3.connect(db_file)
    if need_init:
        try:
            c = conn.cursor()
            with open(config.SQLITE3_SCHEMA_FILE, 'r') as f:
                sql = f.read()
                c.execute(sql)
                conn.commit()
        except Exception as e:
            conn.rollback()
            logging.error(f"Failed to init database: {e}")
            raise e
        finally:
            if c is not None:
                c.close()
    return conn

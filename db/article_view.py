import sqlite3
import time
import logging


def insert(db: sqlite3.Connection, article_url: str, view_count: int):
    sql = 'insert into article_view(url, `count`, create_time) values(?, ?, ?);'
    cursor = db.cursor()
    try:
        cursor.execute(sql, (article_url, view_count, int(time.time())))
        db.commit()
    except Exception as e:
        logging.error(f'Failed to insert article view: {e}')
        raise e
    finally:
        if cursor is not None:
            cursor.close()


def get_last_view_count(db: sqlite3.Connection, article_url: str):
    sql = 'select `count` from article_view where url=? order by create_time desc limit 1;'
    cursor = db.cursor()
    try:
        cursor.execute(sql, (article_url,))
        result = cursor.fetchone()
        return None if result is None else result[0]
    except Exception as e:
        logging.error(f'Failed to insert article view: {e}')
        raise e
    finally:
        if cursor is not None:
            cursor.close()
